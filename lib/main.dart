import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:mokumokuren/src/app.dart';

void main() async {
  LocalStorage storage = LocalStorage('movies_infos.json');

  await storage.ready;

  runApp(MaterialApp(
    home: MyApp(storage: storage),
    theme: ThemeData(
      brightness: Brightness.dark,
      fontFamily: 'Luciole',
    ),
  ));
}