import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:mokumokuren/src/tabs/today.dart';

class MyApp extends StatelessWidget {
  final LocalStorage storage;

  MyApp({Key? key, required this.storage}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("UGC Cine Centre"),
      ),
      body: TodayView(),
      bottomNavigationBar: BottomNavigationBar(
        iconSize: 28.0,
        items: [
          BottomNavigationBarItem(
            label: 'Today',
            icon: Icon(Icons.weekend)
          ),
          BottomNavigationBarItem(
            label: 'Movies',
            icon: Icon(Icons.amp_stories)
          ),
        ],
      ),
    );
  }
}