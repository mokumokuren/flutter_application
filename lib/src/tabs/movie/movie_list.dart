import 'package:mokumokuren/src/tabs/movie/movie_entity.dart';

class MovieList {
  late final List<Movie> movies;

  int get length => movies.length;

  MovieList(this.movies);

  MovieList.fromJSON(Map<String, dynamic> json) {
    movies = [];
  }

  Movie operator[](index) => movies[index];  
}