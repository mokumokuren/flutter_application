class Movie {
  final String movie_id;
  final String title;
  final List<String> sessions;
  final String poster_uri;

  final String? trailer_uri;
  // Not implemented yet
  final String? synopsis;

  const Movie({
    required this.movie_id,
    required this.title,
    required this.sessions,
    required this.poster_uri,

    this.trailer_uri = null,
    this.synopsis = null
  });
}