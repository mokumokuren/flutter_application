import 'dart:convert';
import 'dart:math';
import 'package:http/http.dart' as http;

import 'package:flutter/material.dart';
import './movie/movie_list.dart';
import './movie/movie_entity.dart';

Future<Map<String, dynamic>> _getJsonFromServer() async {
  Uri uri = Uri.parse(TodayView.TODAY_INFOS_URL);

  http.Response response = await http.get(uri); 

  Map<String, dynamic> json = jsonDecode(utf8.decode(response.bodyBytes));

  return json;
}

class TodayView extends StatefulWidget {
  static const String TODAY_INFOS_URL = "http://localhost:8080/today";

  @override
  State<TodayView> createState() => _TodayViewState();
}

class _TodayViewState extends State<TodayView> {
  late MovieList _movies;

  @override
  initState() async {
    super.initState();

    _movies = MovieList.fromJSON(
      await _getJsonFromServer()
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.separated(
        padding: EdgeInsets.all(10.0),

        separatorBuilder: (context, index) => SizedBox(height: 30.0),
        itemCount: _movies.length,
        itemBuilder: (_, index) => _generateMovieView(_movies[index])
      ), 
    );
  }

  Widget _generateMovieView(Movie movie) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.grey[800],
        border: Border.all(
          color: Colors.grey[850]!
        ),
        borderRadius: BorderRadius.circular(5.0),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.grey[900]!,
            offset: Offset.fromDirection(pi*0.5, 5.0),
            blurRadius: 10.0,
          )
        ]
      ),
      padding: EdgeInsets.all(15.0),
      child: _movieWidget(movie),
    );
  }

  Widget _movieWidget(Movie movie) {
    return Row(
      children: [
        Expanded(
          flex: 1,
          child: Image.network(movie.poster_uri)
        ),
        SizedBox(
          width: 5,
        ),
        Expanded(
          flex: 2,
          child: Column(
            children: [
              Container(
                alignment: Alignment.bottomLeft,
                padding: EdgeInsets.symmetric(
                  vertical: 20.0,
                  horizontal: 5.0
                ),
                child: Text(
                  movie.title,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: 24.0,
                    fontWeight: FontWeight.w400,
                  ),
                )
              ),
              SizedBox(
                width: double.infinity,
                child: Wrap(
                  alignment: WrapAlignment.center,
                  spacing: 10.0,
                  runSpacing: 10.0,
                  children: _generateSessionsWidget(movie.sessions)
                )
              )
            ],
          )
        )
      ]
    );
  }

  List<Widget> _generateSessionsWidget(List<String> sessions) {
    return sessions.map((session) {
      return Container(
        decoration: BoxDecoration(
          color: Colors.grey[700], 
          borderRadius: BorderRadius.circular(3.0)
        ),
        padding: EdgeInsets.all(5.0),
        child: Text(
          session,
          style: TextStyle(
            fontSize: 12.0,
            fontWeight: FontWeight.w500,
          )
        ),
      );
    }).toList();
  }
}